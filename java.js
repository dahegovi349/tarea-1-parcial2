const bd =[
    {"Id":0,"Cédula":"123469788-0","Apellido":"Bautista", "Nombre":"José", "Curso": "Cuarto", 
    "Paralelo":"B", "Direccion":"Portoviejo", "Telefono": "0961308918",
    "Correo":"josebautista@gmail.com"},
    {"Id":1,"Cédula":"136969759-1","Apellido":"Holguin", "Nombre":"Anthony", "Curso": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0979607457",
    "Correo":"anthony_holguin@gmail.com"},
    {"Id":2,"Cédula":"138469788-9","Apellido":"Bermudez", "Nombre":"Jordan", "Curso": "Octavo", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0995541316",
    "Correo":"eddy@gmail.com"},
    {"Id":3,"Cédula":"131469788-5","Apellido":"Macías", "Nombre":"Efraín", "Curso": "Segundo", 
    "Paralelo":"C", "Direccion":"Manta", "Telefono": "0992403464",
    "Correo":"efrainvera2000@gmail.com"},
    {"Id":4,"Cédula":"135059759-5","Apellido":"Gorozabel", "Nombre":"Dalember", "Curso": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0969302834",
    "Correo":"dahegovi349@gmail.com"},
    {"Id":5,"Cédula":"135069788-6","Apellido":"Cuesta", "Nombre":"Sophia", "Curso": "Primero", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0994645977",
    "Correo":"sophi2006@gmail.com"},
    {"Id":6,"Cédula":"136469798-3","Apellido":"Cevallos", "Nombre":"Andrés", "Curso": "Séptimo", 
    "Paralelo":"A", "Direccion":"Montecristi", "Telefono": "0987654321",
    "Correo":"cevallos3000@gmail.com"},
    {"Id":7,"Cédula":"100469725-0","Apellido":"Soledispa", "Nombre":"Jonathan", "Curso": "Octavo", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0990076999",
    "Correo":"jonathanmiguel1999@gmail.com"},
    {"Id":8,"Cédula":"131134217-2","Apellido":"Vinces", "Nombre":"Yuli", "Curso": "Noveno", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0967503210",
    "Correo":"yunavi6@gmail.com"},
    {"Id":9,"Cédula":"123477756-7","Apellido":"Correa", "Nombre":"Rafael", "Curso": "Quinto", 
    "Paralelo":"A", "Direccion":"Quito", "Telefono": "098693147",
    "Correo":"eddy@gmail.com"},
]

const estudiantes = document.querySelectorAll('.nombre-estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Cédula:</h2>
                                            <center><p>${estudiante.Cédula}</p></center>
                                        </div>
                                        <div class="nom">
                                        <h2>Nombre:</h2>
                                            <center><p>${estudiante.Nombre}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Apellido:</h2>
                                            <center><p>${estudiante.Apellido}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <center><p>${estudiante.Correo}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Telefono:</h2>
                                            <center><p>${estudiante.Telefono}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <center><p>${estudiante.Direccion}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Curso:</h2>
                                            <center><p>${estudiante.Curso}</p></center>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <center><p>${estudiante.Paralelo}</p></center>
                                        </div> 
                                    </div>`

            }
        })
    })
})